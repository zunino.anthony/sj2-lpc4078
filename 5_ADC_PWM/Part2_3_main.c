#include <stdio.h>

#include "FreeRTOS.h"
#include "adc.h"
#include "pwm1.h"
#include "queue.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

// This is the queue handle we will need for the xQueue Send/Receive API
static QueueHandle_t adc_to_pwm_task_queue;

// 'static' to make these functions 'private' to this file
/*static void create_blinky_tasks(void);
static void create_uart_task(void);
static void blink_task(void *params);
static void uart_task(void *params);*/
void pwm_task(void *p);
void adc_task(void *p);
static void pin_configure_pwm_channel_as_io_pin(void);
static void pin_configure_adc_channel_as_io_pin(void);
static void PWM__PrintADCReading_MR0_MRx(int adc_reading, pwm1_channel_e pwm1_channel);

int main(void) {
  // create_blinky_tasks();
  // create_uart_task();

  // If you have the ESP32 wifi module soldered on the board, you can try uncommenting this code
  // See esp32/README.md for more details
  // uart3_init();                                                                     // Also include:  uart3_init.h
  // xTaskCreate(esp32_tcp_hello_world_task, "uart3", 1000, NULL, PRIORITY_LOW, NULL); // Include esp32_task.h

  adc_to_pwm_task_queue = xQueueCreate(1, sizeof(int));

  xTaskCreate(pwm_task, "RGB_PWM", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(adc_task, "RGB_ADC", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}

void pwm_task(void *p) {
  const pwm1_channel_e pwm1_channel = PWM1__2_1;
  pwm1__init_single_edge(120);
  pin_configure_pwm_channel_as_io_pin();
  // We only need to set PWM configuration once, and the HW will drive
  // the GPIO at 1000Hz, and control set its duty cycle to 50%
  pwm1__set_duty_cycle(pwm1_channel, 50);
  int percent = 0;
  int adc_reading = 0;
  const int max_adc_int = 4095;

  while (1) {
    // Implement code to receive potentiometer value from queue
    if (xQueueReceive(adc_to_pwm_task_queue, &adc_reading, 100))
      PWM__PrintADCReading_MR0_MRx(adc_reading, pwm1_channel);

    percent = (adc_reading * 100) / max_adc_int;
    pwm1__set_duty_cycle(pwm1_channel, percent);
  }
}

void adc_task(void *p) {
  adc__initialize();
  adc__enable_burst_mode();
  pin_configure_adc_channel_as_io_pin();
  // float sensor_value = 0;
  int adc_reading = 0;
  float adc_voltage = 0;

  while (1) {
    adc_reading = adc__get_channel_reading_with_burst_mode(ADC__CHANNEL_5);
    adc_voltage = (adc_reading * 3.3) / 4095; // find voltage given Vref=3.3V
    if (xQueueSend(adc_to_pwm_task_queue, &adc_reading, 0))
      printf("ADC adc_reading=%d, adc_voltage=%0.3f *** ", adc_reading, adc_voltage);
    else
      puts("didn't send data :(");

    vTaskDelay(100);
  }
}

static void pin_configure_pwm_channel_as_io_pin(void) {
  gpio__construct_with_function(GPIO__PORT_2, 1, GPIO__FUNCTION_1);
}

static void pin_configure_adc_channel_as_io_pin(void) {
  gpio_s gpioStruc = gpio__construct_with_function(GPIO__PORT_1, 31, GPIO__FUNCTION_3);
  gpio__setup_adc(gpioStruc);
}

static void PWM__PrintADCReading_MR0_MRx(int adc_reading, pwm1_channel_e pwm1_channel) {
  uint32_t MRx;
  uint32_t MR0 = LPC_PWM1->MR0;

  switch (pwm1_channel) {
  case PWM1__2_0:
    MRx = LPC_PWM1->MR1;
    printf("PWM MR0=%lu, MR1=%lu.\n", MR0, MRx);
    break;
  case PWM1__2_1:
    MRx = LPC_PWM1->MR2;
    printf("PWM MR0=%lu, MR2=%lu.\n", MR0, MRx);
    break;
  case PWM1__2_2:
    MRx = LPC_PWM1->MR3;
    printf("PWM MR0=%lu, MR3=%lu.\n", MR0, MRx);
    break;
  case PWM1__2_4:
    MRx = LPC_PWM1->MR5;
    printf("PWM MR0=%lu, MR5=%lu.\n", MR0, MRx);
    break;
  case PWM1__2_5:
    MRx = LPC_PWM1->MR6;
    printf("PWM MR0=%lu, MR6=%lu.\n", MR0, MRx);
    break;
  default:
    break;
  }
}
