#include <stdio.h>

#include "FreeRTOS.h"
#include "adc.h"
#include "pwm1.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

// 'static' to make these functions 'private' to this file
/*static void create_blinky_tasks(void);
static void create_uart_task(void);
static void blink_task(void *params);
static void uart_task(void *params);*/
void pwm_task(void *p);
void adc_task(void *p);
static void pin_configure_pwm_channel_as_io_pin(void);
static void pin_configure_adc_channel_as_io_pin(void);

int main(void) {
  // create_blinky_tasks();
  // create_uart_task();

  // If you have the ESP32 wifi module soldered on the board, you can try uncommenting this code
  // See esp32/README.md for more details
  // uart3_init();                                                                     // Also include:  uart3_init.h
  // xTaskCreate(esp32_tcp_hello_world_task, "uart3", 1000, NULL, PRIORITY_LOW, NULL); // Include esp32_task.h

  xTaskCreate(pwm_task, "RGB_PWM", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(adc_task, "RGB_ADC", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();

  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}

void pwm_task(void *p) {
  pwm1__init_single_edge(120);

  // Locate a GPIO pin that a PWM channel will control
  // NOTE You can use gpio__construct_with_function() API from gpio.h
  // TODO Write this function yourself
  pin_configure_pwm_channel_as_io_pin();

  // We only need to set PWM configuration once, and the HW will drive
  // the GPIO at 1000Hz, and control set its duty cycle to 50%
  pwm1__set_duty_cycle(PWM1__2_1, 50);

  // Continue to vary the duty cycle in the loop
  uint8_t percent = 0;
  while (1) {
    pwm1__set_duty_cycle(PWM1__2_1, percent);

    if (++percent > 100) {
      percent = 0;
    }

    vTaskDelay(50);
  }
}

void adc_task(void *p) {
  adc__initialize();
  adc__enable_burst_mode();
  pin_configure_adc_channel_as_io_pin();
  float result = 0;
  while (1) {
    result = adc__get_channel_reading_with_burst_mode(ADC__CHANNEL_5);
    printf("ADC burst Result: %.1f\n", result);
    vTaskDelay(100);
  }
}

static void pin_configure_pwm_channel_as_io_pin(void) {
  gpio__construct_with_function(GPIO__PORT_2, 1, GPIO__FUNCTION_1);
}

static void pin_configure_adc_channel_as_io_pin(void) {
  gpio_s gpioStruc = gpio__construct_with_function(GPIO__PORT_1, 31, GPIO__FUNCTION_3);
  gpio__setup_adc(gpioStruc);
}
