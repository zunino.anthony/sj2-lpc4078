#include <stdio.h>

#include "FreeRTOS.h"
#include "gpio.h"
#include "ssp2_lab.h"
#include "task.h"

void spi_task(void *p);
static void SPI_main__print_signature(adesto_flash_id_s data);

//______________________________________________________

void SPI_main(void) {

  xTaskCreate(spi_task, "SPI", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();
}

void spi_task(void *p) {
  const uint32_t spi_clock_mhz = 24;
  ssp2_lab__init(spi_clock_mhz);

  // From the LPC schematics pdf, find the pin numbers connected to flash memory
  // Read table 84 from LPC User Manual and configure PIN functions for SPI2 pins
  // You can use gpio__construct_with_function() API from gpio.h
  //
  // Note: Configure only SCK2, MOSI2, MISO2.
  // CS will be a GPIO output pin(configure and setup direction)
  ssp2_lab__config_pin_functions();

  while (1) {
    adesto_flash_id_s id = ssp2_lab__adesto_read_signature();
    SPI_main__print_signature(id);
    vTaskDelay(500);
  }
}

static void SPI_main__print_signature(adesto_flash_id_s data) {
  printf("manufacturer_id: %u, device_id_1: %u, device_id_2: %u, extended_device_id (invalid for AT25SF041): %u\n", data.manufacturer_id,
         data.device_id_1, data.device_id_2, data.extended_device_id);
}
