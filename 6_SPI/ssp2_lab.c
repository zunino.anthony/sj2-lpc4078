// file spi.h
// Edited by Anthony Zunino 9/30/22
#include <stdint.h>
#include <stdio.h>

#include "gpio.h"
#include "lpc40xx.h"
#include "ssp2_lab.h"

static gpio_s ssp2_adesto_gpio;
static gpio_s ssp2_logic_gpio;

void ssp2_lab__init(uint32_t max_clock_mhz) {
  // Refer to LPC User manual and setup the register bits correctly
  // a) Power on Peripheral
  const uint32_t ssp2_peripheral_power = (0b1 << 20);

  LPC_SC->PCONP |= ssp2_peripheral_power;

  // b) Setup control registers CR0 and CR1
  const uint32_t ssp2_CR0_DSS_8bit = (0x7 << 0);
  const uint32_t ssp2_CR0_FRF_SPI = (0x0 << 4);
  const uint32_t ssp2_CR0_SCR_0 = (0x00 << 8);
  const uint32_t ssp2_CR1_SSE_SPIEnable = (0x1 << 1);

  LPC_SSP2->CR0 = ssp2_CR0_DSS_8bit | ssp2_CR0_FRF_SPI | ssp2_CR0_SCR_0;
  LPC_SSP2->CR1 = ssp2_CR1_SSE_SPIEnable;

  // c) Setup prescalar register to be <= max_clock_mhz
  // PCLK / (CPSDVSR * [SCR+1]), where PCLK=96MHz, SCR=0, CPSDVSR=4 (8 for logic analyzer)
  const uint32_t clkDivider = (96 / max_clock_mhz);
  const uint32_t ssp2_CPSR_CPSDVSR = (clkDivider << 0);

  if ((clkDivider % 2 == 0) || (clkDivider >= 2) || (clkDivider <= 256))
    LPC_SSP2->CPSR = ssp2_CPSR_CPSDVSR;
  else
    LPC_SSP2->CPSR = (0x4 << 0); // default to 24MHz if bad user entry
}

static void ssp2_lab__wait_until_not_busy(void) {
  const uint32_t ssp2_SR_BSY_bit = (0x1 << 4);
  while (LPC_SSP2->SR & ssp2_SR_BSY_bit) {
    ;
  }
}

uint8_t ssp2_lab__exchange_byte(uint8_t data_out) {
  // Configure the Data register(DR) to send and receive data by checking the SPI peripheral status register
  LPC_SSP2->DR = data_out;
  ssp2_lab__wait_until_not_busy();
  const uint8_t data_in = LPC_SSP2->DR;
  return data_in;
}

void ssp2_lab__config_pin_functions(void) {
  const uint32_t port0_trig_pin = 6;
  const uint32_t port1_cs_pin = 10;
  const uint32_t port1_CLK2_pin = 0;
  const uint32_t port1_MOSI2_pin = 1;
  const uint32_t port1_MISO2_pin = 4;

  ssp2_logic_gpio = gpio__construct_as_output(GPIO__PORT_0, port0_trig_pin);
  ssp2_adesto_gpio = gpio__construct_as_output(GPIO__PORT_1, port1_cs_pin);
  gpio__construct_with_function(GPIO__PORT_1, port1_CLK2_pin, GPIO__FUNCTION_4);
  gpio__construct_with_function(GPIO__PORT_1, port1_MOSI2_pin, GPIO__FUNCTION_4);
  gpio__construct_with_function(GPIO__PORT_1, port1_MISO2_pin, GPIO__FUNCTION_4);
}

// static void ssp2_lab__adesto_cs(void) { gpio__reset(ssp2_adesto_gpio); }

// static void ssp2_lab__adesto_ds(void) { gpio__set(ssp2_adesto_gpio); }

static void ssp2_lab__adesto_cs_trigger(void) {
  gpio__reset(ssp2_adesto_gpio);
  gpio__reset(ssp2_logic_gpio);
}

static void ssp2_lab__adesto_ds_trigger(void) {
  gpio__set(ssp2_adesto_gpio);
  gpio__set(ssp2_logic_gpio);
}

// TODO: Implement the code to read Adesto flash memory signature
adesto_flash_id_s ssp2_lab__adesto_read_signature(void) {
  adesto_flash_id_s data = {0};
  const uint8_t opcodeByte = 0x9F;
  const uint8_t dummyByte = 0x00;

  ssp2_lab__adesto_cs_trigger();
  { // Send opcode and read bytes
    ssp2_lab__exchange_byte(opcodeByte);
    data.manufacturer_id = ssp2_lab__exchange_byte(dummyByte);
    data.device_id_1 = ssp2_lab__exchange_byte(dummyByte);
    data.device_id_2 = ssp2_lab__exchange_byte(dummyByte);
    data.extended_device_id = ssp2_lab__exchange_byte(dummyByte);
  }
  ssp2_lab__adesto_ds_trigger();

  return data;
}
