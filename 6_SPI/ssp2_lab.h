// file gpio_lab.h
// Edited by Anthony Zunino 9/30/22
#pragma once

#include <stdbool.h>

typedef struct {
  uint8_t manufacturer_id;
  uint8_t device_id_1;
  uint8_t device_id_2;
  uint8_t extended_device_id;
} adesto_flash_id_s;

void ssp2_lab__init(uint32_t max_clock_mhz);

// Configure the Data register(DR) to send and receive data by checking the SPI peripheral status register
uint8_t ssp2_lab__exchange_byte(uint8_t data_out);

void ssp2_lab__config_pin_functions(void);

adesto_flash_id_s ssp2_lab__adesto_read_signature(void);