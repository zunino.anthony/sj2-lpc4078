#include <stdio.h>

#include "FreeRTOS.h"
#include "gpio.h"
#include "semphr.h"
#include "ssp2_lab.h"
#include "task.h"

void spi_task(void *p);
// void turn_cs_on_off_task(void *p);
void spi_id_verification_task(void *p);
static void SPI_main__print_signature(adesto_flash_id_s data);

static SemaphoreHandle_t spi_bus_mutex = NULL;

//______________________________________________________

void SPI_main(void) {
  // Initialize your SPI, its pins, and Adesto flash CS GPIO
  const uint32_t spi_clock_mhz = 12;
  ssp2_lab__init(spi_clock_mhz);
  ssp2_lab__config_pin_functions();
  spi_bus_mutex = xSemaphoreCreateMutex();

  // Create two tasks that will continously read signature
  xTaskCreate(spi_id_verification_task, "SPI_Ver1", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(spi_id_verification_task, "SPI_Ver2", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  // xTaskCreate(turn_cs_on_off_task, "SPI_Ver3", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  // xTaskCreate(spi_task, "SPI", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();
}

void spi_task(void *p) {
  while (1) {
    const adesto_flash_id_s id = ssp2_lab__adesto_read_signature();
    SPI_main__print_signature(id);
    vTaskDelay(500);
  }
}

void spi_id_verification_task(void *p) {
  while (1) {
    if (xSemaphoreTake(spi_bus_mutex, 1000)) {
      const adesto_flash_id_s id = ssp2_lab__adesto_read_signature();
      SPI_main__print_signature(id);
      xSemaphoreGive(spi_bus_mutex);

      // When we read a manufacturer ID we do not expect, we will kill this task
      if (0x1F != id.manufacturer_id) {
        fprintf(stderr, "Manufacturer ID read failure\n");
        vTaskSuspend(NULL); // Kill this task
      }
    } else
      puts("did not receive semaphore");
  }
}

static void SPI_main__print_signature(adesto_flash_id_s data) {
  printf("manufacturer_id: %u, device_id_1: %u, device_id_2: %u, extended_device_id (invalid for AT25SF041): %u\n",
         data.manufacturer_id, data.device_id_1, data.device_id_2, data.extended_device_id);
}

#if 0
void turn_cs_on_off_task(void *p) {
  const uint32_t port0_trig_pin = 6;
  static gpio_s ssp2_logic_gpio;
  ssp2_logic_gpio = gpio__construct_as_output(GPIO__PORT_0, port0_trig_pin);
  while (1) {
    gpio__toggle(ssp2_logic_gpio);
    vTaskDelay(500);
  }
}
#endif
