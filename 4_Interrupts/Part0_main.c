#include <stdio.h>

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "gpio.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

// 'static' to make these functions 'private' to this file
void gpio_interrupt(void);

void delay__ms();

void gpio__toggle(gpio_s gpio);
bool gpio__get();
gpio_s gpio__construct_as_input(gpio__port_e port, uint8_t pin_number_0_to_31);  ///< construct then set as input
gpio_s gpio__construct_as_output(gpio__port_e port, uint8_t pin_number_0_to_31); ///< construct then set as output
void gpio__enable_pull_down_resistors(gpio_s gpio);

// Step 1:

int main(void) {
  gpio_s LED1_p1_26 = gpio__construct_as_output(GPIO__PORT_1, 26);

  // Read Table 95 in the LPC user manual and setup an interrupt on a switch connected to Port0 or Port2
  // a) For example, choose SW2 (P0_30) pin on SJ2 board and configure as input
  //.   Warning: P0.30, and P0.31 require pull-down resistors
  gpio_s SW2_p0_30 = gpio__construct_as_input(GPIO__PORT_0, 30);
  gpio__enable_pull_down_resistors(SW2_p0_30);

  // b) Configure the registers to trigger Port0 interrupt (such as falling edge)
  LPC_GPIOINT->IO0IntEnF |= (1 << 30); // enable falling edge for port0, pin 30

  // Install GPIO interrupt function at the CPU interrupt (exception) vector
  // c) Hijack the interrupt vector at interrupt_vector_table.c and have it call our gpio_interrupt()
  //    Hint: You can declare 'void gpio_interrupt(void)' at interrupt_vector_table.c such that it can see this function
  // gpio_interrupt();

  // Most important step: Enable the GPIO interrupt exception using the ARM Cortex M API (this is from lpc40xx.h)
  NVIC_EnableIRQ(GPIO_IRQn);

  // Toggle an LED in a loop to ensure/test that the interrupt is entering ane exiting
  // For example, if the GPIO interrupt gets stuck, this LED will stop blinking
  while (1) {
    delay__ms(100);
    // if (gpio__get(SW2_p0_30))
    gpio__toggle(LED1_p1_26);
  }
  return 0;
}

// Step 2:
void gpio_interrupt(void) {
  // a) Clear Port0/2 interrupt using CLR0 or CLR2 registers
  LPC_GPIOINT->IO0IntClr |= (1 << 30); // clear interrupt for port0, pin 30

  // b) Use fprintf(stderr) or blink and LED here to test your ISR
  fprintf(stderr, "Hit Interrupt\n");
}
