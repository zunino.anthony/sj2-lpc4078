#include <stdio.h>

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "gpio.h"
#include "lpc_peripherals.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

static SemaphoreHandle_t switch_pressed_signal;

void delay__ms();
void gpio_interrupt(void);

static void sleep_on_sem_task(void *p);
static void configure_your_gpio_interrupt(void);
static void clear_gpio_interrupt(void);

int main(void) {
  switch_pressed_signal = xSemaphoreCreateBinary(); // Create your binary semaphore

  configure_your_gpio_interrupt();
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio_interrupt, "gpio_interrupt");
  NVIC_EnableIRQ(GPIO_IRQn); // Enable interrupt gate for the GPIO

  xTaskCreate(sleep_on_sem_task, "sem", (512U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();

  return 0;
}

// WARNING: You can only use printf(stderr, "foo") inside of an ISR
void gpio_interrupt(void) {
  fprintf(stderr, "ISR Entry\n");
  xSemaphoreGiveFromISR(switch_pressed_signal, NULL);
  clear_gpio_interrupt();
}

static void sleep_on_sem_task(void *p) {
  gpio_s LED1_p1_26 = gpio__construct_as_output(GPIO__PORT_1, 26);
  while (1) {
    // Use xSemaphoreTake with forever delay and blink an LED when you get the signal
    if (xSemaphoreTake(switch_pressed_signal, portMAX_DELAY))
      gpio__toggle(LED1_p1_26);
  }
}

static void configure_your_gpio_interrupt() {
  // Setup an interrupt on a switch connected to SW2 P0_30 on SJ2 board and configure as input
  // Warning: P0.30 requires pull-down resistor
  gpio_s SW2_p0_30 = gpio__construct_as_input(GPIO__PORT_0, 30);
  gpio__enable_pull_down_resistors(SW2_p0_30);

  // Configure the registers to trigger Port0 interrupt (such as falling edge)
  LPC_GPIOINT->IO0IntEnF |= (1 << 30); // enable falling edge for port0, pin 30
}

static void clear_gpio_interrupt(void) {
  LPC_GPIOINT->IO0IntClr |= (1 << 30); // clear interrupt for port0, pin 30 using CLR0 register
}
