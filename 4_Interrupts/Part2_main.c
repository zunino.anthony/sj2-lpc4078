#include <stdio.h>

#include "FreeRTOS.h"
#include "gpio_isr.h"
#include "semphr.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

static SemaphoreHandle_t switch_pressed_signal_LED1;
static SemaphoreHandle_t switch_pressed_signal_LED0;

void delay__ms();
void sleep_on_sem_togLED1(void *p);
void sleep_on_sem_togLED0(void *p);

void pin30_isr(void);
void pin29_isr(void);

void gpio0__attach_interrupt(uint32_t pin, gpio_interrupt_e interrupt_type, function_pointer_t callback);
void gpio0__interrupt_dispatcher(void);

int main(void) {
  switch_pressed_signal_LED1 = xSemaphoreCreateBinary(); // Create your binary semaphore
  switch_pressed_signal_LED0 = xSemaphoreCreateBinary(); // Create your binary semaphore

  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio0__interrupt_dispatcher, "gpio0__interrupt_dispatcher");
  NVIC_EnableIRQ(GPIO_IRQn); // Enable interrupt gate for the GPIO

  gpio0__attach_interrupt(30, GPIO_INTR__RISING_EDGE, pin30_isr);
  gpio0__attach_interrupt(29, GPIO_INTR__FALLING_EDGE, pin29_isr);

  xTaskCreate(sleep_on_sem_togLED1, "sem", (512U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(sleep_on_sem_togLED0, "sem", (512U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();

  return 0;
}

// WARNING: You can only use printf(stderr, "foo") inside of an ISR
void pin30_isr(void) {
  fprintf(stderr, "Func Callback pin30 ISR Entry\n");
  xSemaphoreGiveFromISR(switch_pressed_signal_LED0, NULL);
}
void pin29_isr(void) {
  fprintf(stderr, "Func Callback pin29 ISR Entry\n");
  xSemaphoreGiveFromISR(switch_pressed_signal_LED1, NULL);
}

void sleep_on_sem_togLED1(void *p) {
  gpio_s LED1_p1_26 = gpio__construct_as_output(GPIO__PORT_1, 26);
  while (1) {
    // Use xSemaphoreTake with forever delay and blink an LED when you get the signal
    if (xSemaphoreTake(switch_pressed_signal_LED1, portMAX_DELAY))
      gpio__toggle(LED1_p1_26);
  }
}

void sleep_on_sem_togLED0(void *p) {
  gpio_s LED0_p2_3 = gpio__construct_as_output(GPIO__PORT_2, 3);
  while (1) {
    // Use xSemaphoreTake with forever delay and blink an LED when you get the signal
    if (xSemaphoreTake(switch_pressed_signal_LED0, portMAX_DELAY))
      gpio__toggle(LED0_p2_3);
  }
}
