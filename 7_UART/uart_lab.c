// file uart_lab.c
// Edited by Anthony Zunino 10/9/22
#include <stdio.h>

#include "FreeRTOS.h"

#include "gpio.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "queue.h"
#include "uart_lab.h"

static void uart_lab__init_registers(uart_number_e uart, uint32_t peripheral_clock, uint32_t baud_rate);
static void uart_lab__config_pin_functions(uart_number_e uart);
static void uart_lab__wait_until_receive_ready(uart_number_e uart);
static void uart_lab__wait_until_transmit_ready(uart_number_e uart);
static void uart_lab__receive_interrupt(void);

static QueueHandle_t uart_lab__rx_queue;

void uart_lab__init(uart_number_e uart, uint32_t peripheral_clock, uint32_t baud_rate) {
  const uint32_t uart2_peripheral_power = (0x1 << 24); // LPC_PERIPHERAL__UART2
  const uint32_t uart3_peripheral_power = (0x1 << 25); // LPC_PERIPHERAL__UART3

  if (uart == UART_2) // Power on Peripheral
    LPC_SC->PCONP |= uart2_peripheral_power;
  else
    LPC_SC->PCONP |= uart3_peripheral_power;

  uart_lab__init_registers(uart, peripheral_clock, baud_rate); // Setup DLL, DLM, FDR, LCR registers

  uart_lab__config_pin_functions(uart); // Initialize IO pins (rx should not have pull downs)
}

// Read the byte from RBR and actually save it to the pointer
bool uart_lab__polled_get(uart_number_e uart, char *input_byte) {
  uart_lab__wait_until_receive_ready(uart); // a) Check LSR for Receive Data Ready

  if (uart == UART_2) // b) Copy data from RBR register to input_byte
    *input_byte = LPC_UART2->RBR;
  else
    *input_byte = LPC_UART3->RBR;

  return true;
}

bool uart_lab__polled_put(uart_number_e uart, char output_byte) {
  uart_lab__wait_until_transmit_ready(uart); // a) Check LSR for Transmit Hold Register Empty

  if (uart == UART_2) // b) Copy output_byte to THR register
    LPC_UART2->THR = output_byte;
  else
    LPC_UART3->THR = output_byte;

  return true;
}

void uart_lab__enable_receive_interrupt(uart_number_e uart_number) {
  const uint32_t IER_RBRIE_Interrupt_Enable_bit = (0x1 << 0);

  if (uart_number == UART_2) {
    lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__UART2, uart_lab__receive_interrupt, "uart_lab__receive_interrupt");
    LPC_UART2->IER = IER_RBRIE_Interrupt_Enable_bit;
    uart_lab__rx_queue = xQueueCreate(16, sizeof(char));
  } else {
    lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__UART3, uart_lab__receive_interrupt, "uart_lab__receive_interrupt");
    LPC_UART3->IER = IER_RBRIE_Interrupt_Enable_bit;
    uart_lab__rx_queue = xQueueCreate(16, sizeof(char));
  }
}

bool uart_lab__get_char_from_queue(char *input_byte, uint32_t timeout) {
  return xQueueReceive(uart_lab__rx_queue, input_byte, timeout);
}

static void uart_lab__receive_interrupt(void) {
  const uint32_t IIR_INTSTATUS_bit = (0x1 << 0);
  const uint32_t IIR_INTID_bit = (0x2 << 1);

  if (LPC_UART3->IIR & IIR_INTSTATUS_bit)
    return;
  if (!(LPC_UART3->IIR & IIR_INTID_bit))
    return;

  uart_lab__wait_until_receive_ready(UART_3);
  const char byte = LPC_UART3->RBR;
  xQueueSendFromISR(uart_lab__rx_queue, &byte, NULL);
}

static void uart_lab__init_registers(uart_number_e uart, uint32_t peripheral_clock, uint32_t baud_rate) {
  const uint32_t divider_16_bit =
      peripheral_clock * 1000 * 1000 / (16 * baud_rate); // Baud = PCLK / 16 * (divider_16_bit)
  const uint32_t wls_bit = (0x3 << 0);                   // 8-bit transfer
  const uint32_t dlab_bit = (0x1 << 7);                  // access DLL/DLM for baud rate
  const uint32_t divaddval_bit = (0x0 << 0);             // disable fraction baud rate
  const uint32_t mulval_bit = (0x1 << 4);                // must be >=1 for UART to work

  if (uart == UART_2) {
    LPC_UART2->LCR = dlab_bit;
    LPC_UART2->DLM = (divider_16_bit >> 8) & 0xFF;
    LPC_UART2->DLL = (divider_16_bit >> 0) & 0xFF;
    LPC_UART2->LCR &= ~dlab_bit;
    LPC_UART2->FDR = divaddval_bit | mulval_bit;
    LPC_UART2->LCR |= wls_bit;
  } else {
    LPC_UART3->LCR = dlab_bit;
    LPC_UART3->DLM = (divider_16_bit >> 8) & 0xFF;
    LPC_UART3->DLL = (divider_16_bit >> 0) & 0xFF;
    LPC_UART3->LCR &= ~dlab_bit;
    LPC_UART3->FDR = divaddval_bit | mulval_bit;
    LPC_UART3->LCR |= wls_bit;
  }
}

static void uart_lab__config_pin_functions(uart_number_e uart) {
  const uint16_t uart2_port2_8_tx = 8;
  const uint16_t uart2_port2_9_rx = 9;
  const uint16_t uart3_port4_28_tx = 28;
  const uint16_t uart3_port4_29_rx = 29;

  if (uart == UART_2) {
    gpio__construct_with_function(GPIO__PORT_2, uart2_port2_8_tx, GPIO__FUNCTION_2);
    gpio_s uart2_rx_pin = gpio__construct_with_function(GPIO__PORT_2, uart2_port2_9_rx, GPIO__FUNCTION_2);
    gpio__disable_pull_down_resistors(uart2_rx_pin);
  } else {
    gpio__construct_with_function(GPIO__PORT_4, uart3_port4_28_tx, GPIO__FUNCTION_2);
    gpio_s uart3_rx_pin = gpio__construct_with_function(GPIO__PORT_4, uart3_port4_29_rx, GPIO__FUNCTION_2);
    gpio__disable_pull_down_resistors(uart3_rx_pin);
  }
}

static void uart_lab__wait_until_receive_ready(uart_number_e uart) {
  const uint32_t uart_LSR_rx_data_ready_bit = (0x1 << 0);
  if (uart == UART_2) {
    while (!(LPC_UART2->LSR & uart_LSR_rx_data_ready_bit)) {
      ;
    }
  } else {
    while (!(LPC_UART3->LSR & uart_LSR_rx_data_ready_bit)) {
      ;
    }
  }
}

static void uart_lab__wait_until_transmit_ready(uart_number_e uart) {
  const uint32_t uart_LSR_tx_hold_reg_empty_bit = (0x1 << 5);
  if (uart == UART_2) {
    while (!(LPC_UART2->LSR & uart_LSR_tx_hold_reg_empty_bit)) {
      ;
    }
  } else {
    while (!(LPC_UART3->LSR & uart_LSR_tx_hold_reg_empty_bit)) {
      ;
    }
  }
}
