#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"
#include "uart_lab.h"

static void board_1_sender_task(void *p);
static void board_2_receiver_task(void *p);
static void uart_read_task(void *p);
static void uart_write_task(void *p);

static uart_number_e uart = UART_3;
static uart_number_e uart_tx = UART_2;
static uart_number_e uart_rx = UART_3;

//______________________________________________________

void UART_main(void) {
  const uint32_t peripheral_clock = 96;
  const uint32_t baud_rate = 9600;
  // uart_lab__init(uart_tx, peripheral_clock, baud_rate);
  uart_lab__init(uart_tx, peripheral_clock, baud_rate);
  uart_lab__init(uart_rx, peripheral_clock, baud_rate);

  uart_lab__enable_receive_interrupt(uart_rx);

  xTaskCreate(board_2_receiver_task, "UART Rx", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(board_1_sender_task, "UART Tx", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();
}

static void board_1_sender_task(void *p) {
  char number_as_string[16] = {0};

  while (true) {
    const int number = rand();
    sprintf(number_as_string, "%i", number);

    // Send one char at a time to the other board including terminating NULL char
    for (int i = 0; i <= strlen(number_as_string); i++) {
      uart_lab__polled_put(uart_tx, number_as_string[i]);
      // printf("Sent: %c\n", number_as_string[i]);
    }

    printf("Sent: %i over UART to the other board\n", number);
    vTaskDelay(3000);
  }
}

static void board_2_receiver_task(void *p) {
  char number_as_string[16] = {0};
  int counter = 0;

  while (true) {
    char byte = 0;
    uart_lab__get_char_from_queue(&byte, portMAX_DELAY);
    // printf("Received: %c\n", byte);

    if ('\0' == byte) { // This is the last char, so print the number
      number_as_string[counter] = '\0';
      counter = 0;
      printf("Received this number from the other board: %s\n", number_as_string);
    }
    // We have not yet received the NULL '\0' char, so buffer the data
    else {
      if (counter < 16) {
        number_as_string[counter++] = byte;
      }
    }
  }
}

static void uart_read_task(void *p) {
  while (1) {
    char input_byte;
    uart_lab__polled_get(uart, &input_byte);
    printf("Polled_get after: %d\n", input_byte);
    vTaskDelay(500);
  }
}

static void uart_write_task(void *p) {
  while (1) {
    char output_byte = 'Z';
    printf("Polled_put: %d\n", output_byte);
    uart_lab__polled_put(uart, output_byte);
    vTaskDelay(500);
  }
}
