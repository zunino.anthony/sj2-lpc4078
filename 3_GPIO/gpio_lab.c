// Edited by Anthony Zunino 9/11/22
#include "gpio_lab.h"
#include "lpc40xx.h"

// uint32_t *addrDirPort1 = (uint32_t*)(0x20098000 + 0x20);
// uint32_t *addrPinPort1 = (uint32_t*)(0x20098000 + 0x34);

// NOTE: The IOCON is not part of this driver
void gpio1__set_as_input(uint8_t pin_num) { /* Should alter the hardware registers to set the pin as input */
  const uint32_t inputPin = (1 << pin_num);
  LPC_GPIO1->DIR &= ~inputPin;
  return;
}

void gpio1__set_as_output(uint8_t pin_num) { /* Should alter the hardware registers to set the pin as output */
  const uint32_t outputPin = (1 << pin_num);
  LPC_GPIO1->DIR |= outputPin;
  return;
}

void gpio1__set_high(uint8_t pin_num) { /* Should alter the hardware registers to set the pin as high, active low */
  const uint32_t setPinHigh = (1 << pin_num);
  LPC_GPIO1->CLR = setPinHigh;
  return;
}

void gpio1__set_low(uint8_t pin_num) { /* Should alter the hardware registers to set the pin as low */
  const uint32_t setPinLow = (1 << pin_num);
  LPC_GPIO1->SET = setPinLow;
  return;
}

/**
 * Should alter the hardware registers to set the pin as low
 *
 * @param {bool} high - true => set pin high, false => set pin low
 */
void gpio1__set(uint8_t pin_num, bool high) {
  if (high) {
    gpio1__set_high(pin_num);
  } else {
    gpio1__set_low(pin_num);
  }
}

/**
 * Should return the state of the pin (input or output, doesn't matter)
 *
 * @return {bool} level of pin high => true, low => false
 */
bool gpio1__get_level(uint8_t pin_num) {
  const uint32_t inputPin = (1 << pin_num);
  if (LPC_GPIO1->MASK & inputPin)
    return true;
  return false;
}