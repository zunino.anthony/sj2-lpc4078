#include <stdio.h>

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

// 'static' to make these functions 'private' to this file
static void create_blinky_tasks(void);
static void create_uart_task(void);
static void blink_task(void *params);
static void uart_task(void *params);

void led_task(void *task_parameter);
void switch_task(void *task_parameter);
void vReadSwitch(void *task_parameter);
void vControlLED(void *task_parameter);
void blinkLED(void *task_parameter);
void toggleLED(void *task_parameter);

void gpiox__set_as_output(uint8_t port_num, uint8_t pin_num);
void gpiox__set_as_input(uint8_t port_num, uint8_t pin_num);
void gpiox__set_high(uint8_t port_num, uint8_t pin_num);
void gpiox__set_low(uint8_t port_num, uint8_t pin_num);
bool gpiox__get_level(uint8_t port_num, uint8_t pin_num);

//_________________________________________________________________

typedef struct {
  /* First get gpio1 driver to work only, and if you finish it
   * you can do the extra credit to also make it work for other Ports
   */
  uint8_t port;
  uint8_t pin;
} port_pin_s;

//_________________________________________________________________

static SemaphoreHandle_t switch_press_indication;

bool globalSwFlag = 0;

//_________________________________________________________________

int main(void) {
  create_blinky_tasks();
  create_uart_task();

  // If you have the ESP32 wifi module soldered on the board, you can try uncommenting this code
  // See esp32/README.md for more details
  // uart3_init();                                                                     // Also include:  uart3_init.h
  // xTaskCreate(esp32_tcp_hello_world_task, "uart3", 1000, NULL, PRIORITY_LOW, NULL); // Include esp32_task.h

  switch_press_indication = xSemaphoreCreateBinary();

  // Hint: Use on-board LEDs first to get this logic to work
  //       After that, you can simply switch these parameters to off-board LED and a switch
  // "SW0" = SW3/P1_19 TODO change sw to 19

  static port_pin_s sw0 = {1, 19}; // SW0 P1_19
  static port_pin_s led0 = {2, 3}; // LED0 P2_3

  static port_pin_s sw1 = {1, 15};  // SW1 P1_15
  static port_pin_s led1 = {1, 26}; // LED1 P1_26

  // GPIO Lab on bookstack - on press, continuosly blink LED0
  xTaskCreate(switch_task, "sw0", 2048 / sizeof(void *), &sw0, PRIORITY_LOW, NULL);
  xTaskCreate(led_task, "led0", 2048 / sizeof(void *), &led0, PRIORITY_HIGH, NULL);

  // GPIO HW on Canvas - on release, toggle LED1
  xTaskCreate(vReadSwitch, "sw1", 2048 / sizeof(void *), &sw1, PRIORITY_LOW, NULL);
  xTaskCreate(vControlLED, "led1", 2048 / sizeof(void *), &led1, PRIORITY_LOW, NULL);

  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}

//_________________________________________________________________

void led_task(void *task_parameter) {
  while (true) {
    // Note: There is no vTaskDelay() here, but we use sleep mechanism while waiting for the binary semaphore (signal)
    if (xSemaphoreTake(switch_press_indication, 1000)) {
      puts("Receiving semaphore! Blinking LED!");
      blinkLED(task_parameter);
    } else
      puts("Timeout: No switch press indication for 1000ms");
  }
}

void switch_task(void *task_parameter) {
  port_pin_s *sw = (port_pin_s *)task_parameter;
  uint8_t pin = sw->pin;
  uint8_t port = sw->port;

  gpiox__set_as_input(port, pin);

  while (true) {
    if (gpiox__get_level(port, pin)) {
      xSemaphoreGive(switch_press_indication);
      puts("Giving semaphore");
    }

    // Task should always sleep otherwise they will use 100% CPU
    // This task sleep also helps avoid spurious semaphore give during switch debeounce

    vTaskDelay(125);
  }
}

void vReadSwitch(void *task_parameter) {
  port_pin_s *sw = (port_pin_s *)task_parameter;
  uint8_t pin = sw->pin;
  uint8_t port = sw->port;

  gpiox__set_as_input(port, pin);
  bool buttonPressed = 0;

  while (true) {
    if (buttonPressed == 1 && gpiox__get_level(port, pin) == 0) {
      puts("Button has been released! Blink LED!");
      globalSwFlag = 1;
      buttonPressed = 0;
      vTaskDelay(50);
    }
    if (gpiox__get_level(port, pin)) {
      puts("Button is being pressed...");
      buttonPressed = 1;
      vTaskDelay(50);
    }

    // Task should always sleep otherwise they will use 100% CPU
    // This task sleep also helps avoid spurious semaphore give during switch debeounce

    vTaskDelay(100);
  }
}

void vControlLED(void *task_parameter) {
  while (true) {
    if (globalSwFlag) {
      puts("Blinking LED!");
      toggleLED(task_parameter);
      globalSwFlag = 0;
    } else
      puts("Timeout: No switch press indication for 1000ms");
    vTaskDelay(1000);
  }
}

void blinkLED(void *task_parameter) {
  // Type-cast the paramter that was passed from xTaskCreate()
  const port_pin_s *led = (port_pin_s *)(task_parameter);

  uint8_t pin = led->pin;
  uint8_t port = led->port;

  gpiox__set_as_output(port, pin);

  gpiox__set_high(port, pin);
  vTaskDelay(250);

  gpiox__set_low(port, pin);
  vTaskDelay(250);
}

void toggleLED(void *task_parameter) {
  // Type-cast the paramter that was passed from xTaskCreate()
  const port_pin_s *led = (port_pin_s *)(task_parameter);

  uint8_t pin = led->pin;
  uint8_t port = led->port;

  gpiox__set_as_output(port, pin);

  if (gpiox__get_level(port, pin)) {
    gpiox__set_high(port, pin);
    vTaskDelay(250);
  } else {
    gpiox__set_low(port, pin);
    vTaskDelay(250);
  }
}

static void create_blinky_tasks(void) {
  /**
   * Use '#if (1)' if you wish to observe how two tasks can blink LEDs
   * Use '#if (0)' if you wish to use the 'periodic_scheduler.h' that will spawn 4 periodic tasks, one for each LED
   */
#if (1)
  // These variables should not go out of scope because the 'blink_task' will reference this memory
  static gpio_s led0, led1;

  // If you wish to avoid malloc, use xTaskCreateStatic() in place of xTaskCreate()
  static StackType_t led0_task_stack[512 / sizeof(StackType_t)];
  static StackType_t led1_task_stack[512 / sizeof(StackType_t)];
  static StaticTask_t led0_task_struct;
  static StaticTask_t led1_task_struct;

  led0 = board_io__get_led2();
  led1 = board_io__get_led3();

  xTaskCreateStatic(blink_task, "led0", ARRAY_SIZE(led0_task_stack), (void *)&led0, PRIORITY_LOW, led0_task_stack,
                    &led0_task_struct);
  xTaskCreateStatic(blink_task, "led1", ARRAY_SIZE(led1_task_stack), (void *)&led1, PRIORITY_LOW, led1_task_stack,
                    &led1_task_struct);
#else
  periodic_scheduler__initialize();
  UNUSED(blink_task);
#endif
}

static void create_uart_task(void) {
  // It is advised to either run the uart_task, or the SJ2 command-line (CLI), but not both
  // Change '#if (0)' to '#if (1)' and vice versa to try it out
#if (0)
  // printf() takes more stack space, size this tasks' stack higher
  xTaskCreate(uart_task, "uart", (512U * 8) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#else
  sj2_cli__init();
  UNUSED(uart_task); // uart_task is un-used in if we are doing cli init()
#endif
}

static void blink_task(void *params) {
  const gpio_s led = *((gpio_s *)params); // Parameter was input while calling xTaskCreate()

  // Warning: This task starts with very minimal stack, so do not use printf() API here to avoid stack overflow
  while (true) {
    gpio__toggle(led);
    vTaskDelay(500);
  }
}

// This sends periodic messages over printf() which uses system_calls.c to send them to UART0
static void uart_task(void *params) {
  TickType_t previous_tick = 0;
  TickType_t ticks = 0;

  while (true) {
    // This loop will repeat at precise task delay, even if the logic below takes variable amount of ticks
    vTaskDelayUntil(&previous_tick, 2000);

    /* Calls to fprintf(stderr, ...) uses polled UART driver, so this entire output will be fully
     * sent out before this function returns. See system_calls.c for actual implementation.
     *
     * Use this style print for:
     *  - Interrupts because you cannot use printf() inside an ISR
     *    This is because regular printf() leads down to xQueueSend() that might block
     *    but you cannot block inside an ISR hence the system might crash
     *  - During debugging in case system crashes before all output of printf() is sent
     */
    ticks = xTaskGetTickCount();
    fprintf(stderr, "%u: This is a polled version of printf used for debugging ... finished in", (unsigned)ticks);
    fprintf(stderr, " %lu ticks\n", (xTaskGetTickCount() - ticks));

    /* This deposits data to an outgoing queue and doesn't block the CPU
     * Data will be sent later, but this function would return earlier
     */
    ticks = xTaskGetTickCount();
    printf("This is a more efficient printf ... finished in");
    printf(" %lu ticks\n\n", (xTaskGetTickCount() - ticks));
  }
}