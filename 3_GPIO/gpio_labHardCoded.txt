// Edited by Anthony Zunino 9/11/22
#include "gpio_lab.h"
#include "lpc40xx.h"

// uint32_t *addrDirPort1 = (uint32_t*)(0x20098000 + 0x20);
// uint32_t *addrPinPort1 = (uint32_t*)(0x20098000 + 0x34);

// NOTE: The IOCON is not part of this driver
void gpio0__set_as_input(uint8_t pin_num) { /* Should alter the hardware registers to set the pin as input */
  uint32_t *addrDirPort1 = (uint32_t *)(0x20098000 + 0x20);
  const uint32_t inputPin = (1 << pin_num);
  *addrDirPort1 &= ~inputPin;
  return;
}

void gpio0__set_as_output(uint8_t pin_num) { /* Should alter the hardware registers to set the pin as output */
  uint32_t *addrDirPort1 = (uint32_t *)(0x20098000 + 0x20);
  const uint32_t outputPin = (1 << pin_num);
  *addrDirPort1 |= outputPin;
  return;
}

void gpio0__set_high(uint8_t pin_num) { /* Should alter the hardware registers to set the pin as high, active low */
  uint32_t *addrPinPort1 = (uint32_t *)(0x20098000 + 0x34);
  const uint32_t inputPin = (1 << pin_num);
  *addrPinPort1 |= inputPin;
  return;
}

void gpio0__set_low(uint8_t pin_num) { /* Should alter the hardware registers to set the pin as low */
  uint32_t *addrPinPort1 = (uint32_t *)(0x20098000 + 0x34);
  const uint32_t inputPin = (1 << pin_num);
  *addrPinPort1 &= ~inputPin;
  return;
}

/**
 * Should alter the hardware registers to set the pin as low
 *
 * @param {bool} high - true => set pin high, false => set pin low
 */
void gpio0__set(uint8_t pin_num, bool high) {
  if (high) {
    gpio0__set_high(pin_num);
  } else {
    gpio0__set_low(pin_num);
  }
}

/**
 * Should return the state of the pin (input or output, doesn't matter)
 *
 * @return {bool} level of pin high => true, low => false
 */
bool gpio0__get_level(uint8_t pin_num) {
  int level = 0;
  uint32_t *addrPinPort1 = (uint32_t *)(0x20098000 + 0x34);
  level |= (*addrPinPort1)[pin_num];
  bool levelState(level);
  return levelState;
}