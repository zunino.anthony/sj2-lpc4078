// file i2c_slave_init.c
// Edited by Anthony Zunino 11/4/22
#include <stdio.h>

#include "FreeRTOS.h"

#include "gpio.h"
#include "i2c.h"
#include "i2c_slave_init.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"

void i2c2__slave_init(uint8_t slave_address_to_respond_to) {

  LPC_I2C2->ADR0 = slave_address_to_respond_to; // use only 4th bit
  LPC_I2C2->MASK0 = 0x00;

  LPC_I2C2->CONSET = 0x44; // enable slave receive + interrupts
}
