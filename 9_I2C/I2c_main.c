#include <stdio.h>
#include <stdlib.h>

#include "FreeRTOS.h"
#include "board_io.h"
#include "i2c_slave_functions.h"
#include "task.h"

#define SLAVE 1
static volatile uint8_t slave_memory[256];
static void useCharArray(void *p);
static void turn_on_an_led(void);
static void turn_off_an_led(void);

void i2c2__slave_init(uint8_t slave_address_to_respond_to);
bool i2c_slave_callback__read_memory(uint8_t memory_index, uint8_t *memory);
bool i2c_slave_callback__write_memory(uint8_t memory_index, uint8_t memory_value);
void gpio__set(gpio_s gpio);
void gpio__reset(gpio_s gpio);

//______________________________________________________

void I2c_main(void) {
  if (SLAVE) {
    i2c2__slave_init(0x86);
  }
  xTaskCreate(useCharArray, "Char Array Task", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();
}

/**
 * Use memory_index and read the data to *memory pointer
 * return true if everything is well
 */
bool i2c_slave_callback__read_memory(uint8_t memory_index, uint8_t *memory) {
  *(memory) = slave_memory[memory_index];
  return true;
}

/**
 * Use memory_index to write memory_value
 * return true if this write operation was valid
 */
bool i2c_slave_callback__write_memory(uint8_t memory_index, uint8_t memory_value) {
  slave_memory[memory_index] = memory_value;
  return true;
}

static void useCharArray(void *p) {
  while (1) {
    if (slave_memory[0] == 5) {
      turn_on_an_led(); // TODO
    } else {
      turn_off_an_led(); // TODO
    }
    vTaskDelay(100);
  }
}

static void turn_on_an_led() { gpio__set(board_io__get_led0()); }

static void turn_off_an_led() { gpio__reset(board_io__get_led0()); }
