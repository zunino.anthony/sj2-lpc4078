#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "FreeRTOS.h"

#include "gpio.h"
#include "queue.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

static QueueHandle_t switch_queue;

typedef enum { switch__off, switch__on } switch_e;

static void consumer_task(void *p);
static void producer_task(void *p);
static switch_e get_switch_input_from_switch0(void);
static void create_blinky_tasks(void);
static void create_uart_task(void);
static void blink_task(void *params);
static void uart_task(void *params);

//______________________________________________________

void PC_main(void) {

  xTaskCreate(producer_task, "producer", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(consumer_task, "consumer", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  switch_queue = xQueueCreate(1, sizeof(switch_e)); // Choose depth of item being our enum (1 should be okay)

  create_blinky_tasks();
  create_uart_task();

  vTaskStartScheduler();
}

static void create_blinky_tasks(void) {
  /**
   * Use '#if (1)' if you wish to observe how two tasks can blink LEDs
   * Use '#if (0)' if you wish to use the 'periodic_scheduler.h' that will spawn 4 periodic tasks, one for each LED
   */
#if (1)
  // These variables should not go out of scope because the 'blink_task' will reference this memory
  static gpio_s led0, led1;

  // If you wish to avoid malloc, use xTaskCreateStatic() in place of xTaskCreate()
  static StackType_t led0_task_stack[512 / sizeof(StackType_t)];
  static StackType_t led1_task_stack[512 / sizeof(StackType_t)];
  static StaticTask_t led0_task_struct;
  static StaticTask_t led1_task_struct;

  led0 = board_io__get_led0();
  led1 = board_io__get_led1();

  xTaskCreateStatic(blink_task, "led0", ARRAY_SIZE(led0_task_stack), (void *)&led0, PRIORITY_LOW, led0_task_stack,
                    &led0_task_struct);
  xTaskCreateStatic(blink_task, "led1", ARRAY_SIZE(led1_task_stack), (void *)&led1, PRIORITY_LOW, led1_task_stack,
                    &led1_task_struct);
#else
  periodic_scheduler__initialize();
  UNUSED(blink_task);
#endif
}

static void create_uart_task(void) {
  // It is advised to either run the uart_task, or the SJ2 command-line (CLI), but not both
  // Change '#if (0)' to '#if (1)' and vice versa to try it out
#if (0)
  // printf() takes more stack space, size this tasks' stack higher
  xTaskCreate(uart_task, "uart", (512U * 8) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#else
  sj2_cli__init();
  UNUSED(uart_task); // uart_task is un-used in if we are doing cli init()
#endif
}

static void producer_task(void *p) {
  while (1) {
    // This xQueueSend() will internally switch context to "consumer" task because
    // it is higher priority than this "producer" task. Then, when the consumer task
    // sleeps, we will resume out of xQueueSend() and go over to the next line

    // TODO: Get some input value from your board
    const switch_e switch_value = get_switch_input_from_switch0();

    // Print a message before/after xQueueSend()
    printf("%s(), line %d, sending message: %d\n", __FUNCTION__, __LINE__, switch_value);
    if (xQueueSend(switch_queue, &switch_value, 0))
      printf("%s(), line %d, sent message\n", __FUNCTION__, __LINE__);
    else
      puts("Failed to send message");

    vTaskDelay(4000);
  }
}

static void consumer_task(void *p) {
  switch_e switch_value;
  while (1) {
    // Print a message before/after xQueueReceive()
    printf("%s(), line %d, receiving message\n", __FUNCTION__, __LINE__);
    if (xQueueReceive(switch_queue, &switch_value, 4000))
      printf("%s(), line %d, received message: %d\n", __FUNCTION__, __LINE__, switch_value);
    else
      puts("Failed to receieve a message");
  }
}

static switch_e get_switch_input_from_switch0(void) {
  // P1_19 is SW0
  uint8_t port = 1;
  uint8_t pin = 19;
  gpio_s sw0 = gpio__construct_as_input(port, pin);

  if (gpio__get(sw0))
    return switch__on;
  else
    return switch__off;
}

static void blink_task(void *params) {
  const gpio_s led = *((gpio_s *)params); // Parameter was input while calling xTaskCreate()

  // Warning: This task starts with very minimal stack, so do not use printf() API here to avoid stack overflow
  while (true) {
    gpio__toggle(led);
    vTaskDelay(500);
  }
}

// This sends periodic messages over printf() which uses system_calls.c to send them to UART0
static void uart_task(void *params) {
  TickType_t previous_tick = 0;
  TickType_t ticks = 0;

  while (true) {
    // This loop will repeat at precise task delay, even if the logic below takes variable amount of ticks
    vTaskDelayUntil(&previous_tick, 2000);

    /* Calls to fprintf(stderr, ...) uses polled UART driver, so this entire output will be fully
     * sent out before this function returns. See system_calls.c for actual implementation.
     *
     * Use this style print for:
     *  - Interrupts because you cannot use printf() inside an ISR
     *    This is because regular printf() leads down to xQueueSend() that might block
     *    but you cannot block inside an ISR hence the system might crash
     *  - During debugging in case system crashes before all output of printf() is sent
     */
    ticks = xTaskGetTickCount();
    fprintf(stderr, "%u: This is a polled version of printf used for debugging ... finished in", (unsigned)ticks);
    fprintf(stderr, " %lu ticks\n", (xTaskGetTickCount() - ticks));

    /* This deposits data to an outgoing queue and doesn't block the CPU
     * Data will be sent later, but this function would return earlier
     */
    ticks = xTaskGetTickCount();
    printf("This is a more efficient printf ... finished in");
    printf(" %lu ticks\n\n", (xTaskGetTickCount() - ticks));
  }
}
