#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "FreeRTOS.h"

#include "gpio.h"
#include "queue.h"
#include "task.h"

static QueueHandle_t switch_queue;

typedef enum { switch__off, switch__on } switch_e;

static void consumer_task(void *p);
static void producer_task(void *p);
static switch_e get_switch_input_from_switch0(void);

//______________________________________________________

void PC_main(void) {

  // TODO: Create your tasks
  xTaskCreate(producer_task, "producer", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(consumer_task, "consumer", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  // TODO Queue handle is not valid until you create it
  switch_queue =
      xQueueCreate(1, sizeof(switch_e)); // Choose depth of item being our enum (1 should be okay for this example)

  vTaskStartScheduler();
}

static void producer_task(void *p) {
  while (1) {
    // This xQueueSend() will internally switch context to "consumer" task because
    // it is higher priority than this "producer" task. Then, when the consumer task
    // sleeps, we will resume out of xQueueSend() and go over to the next line

    // TODO: Get some input value from your board
    const switch_e switch_value = get_switch_input_from_switch0();

    // Print a message before/after xQueueSend()
    printf("%s(), line %d, sending message: %d\n", __FUNCTION__, __LINE__, switch_value);
    xQueueSend(switch_queue, &switch_value, 0);
    printf("%s(), line %d, sent message\n", __FUNCTION__, __LINE__);

    vTaskDelay(1000);
  }
}

static void consumer_task(void *p) {
  switch_e switch_value;
  while (1) {
    // Print a message before/after xQueueReceive()
    printf("%s(), line %d, receiving message\n", __FUNCTION__, __LINE__);
    xQueueReceive(switch_queue, &switch_value, 250);
    printf("%s(), line %d, received message: %d\n", __FUNCTION__, __LINE__, switch_value);
  }
}

static switch_e get_switch_input_from_switch0(void) {
  // P1_19 is SW0
  uint8_t port = 1;
  uint8_t pin = 19;
  gpio_s sw0 = gpio__construct_as_input(port, pin);

  if (gpio__get(sw0))
    return switch__on;
  else
    return switch__off;
}
