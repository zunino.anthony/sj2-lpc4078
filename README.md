# SJ2-LPC4078

This is a project in development started 2/20/24 by **Anthony Zunino**. It contains all the homework and development using the [SJ2](#description) board.

## Name
SJ2-LPC4078

## Description
A simple repo containing the homework projects accumulated in my CMPE244 course at SJSU taught by Preetpal Kang in Fall 2022. All projects led up to the group project of the Autonomous RC Car, [Meh-sla Automotive](http://socialledge.com/sjsu/index.php/S23:_Meh-sla_Automotive).

The car runs using 4 [SJTwo](http://books.socialledge.com/books/embedded-drivers-real-time-operating-systems/page/sj2-board) (SJ2) Rev1.E boards utilizing an **[LPC4078](https://www.nxp.com/docs/en/data-sheet/LPC408X_7X.pdf)** MCU with an ARM Cortex-M4 processor.

The repo is organized into 8 different project folders, each focusing on an individual element. Please refer to the list below:

# NEED TO ADD LINKS TO EACH OF THE PROJECTS

VS RC Car Commands

```
scons --project=motor_controller --no-unit-test
scons --dbc-node-name=DRIVER
python nxp-programmer/flash.py --port  /dev/tty.usbserial-1420 -i _build_motor_controller/motor_controller.bin
python nxp-programmer/flash.py --port  /dev/tty.usbserial-1420
python nxp-programmer/flash.py --port  /dev/tty.usbserial-1410

git pull origin motor_board --rebase
	If you get merge conflicts in the middle of this, fix your merge files and afterwards run “git rebase --continue”
```

### 2_MultipleTasks

### 3_GPIO

### 4_Interrupts

### 5_ADC_PWM

### 6_SPI

### 7_UART

### 8_ProducerConsumer

### 9_I2C



## Test and Deploy

- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)


## Visuals

## Installation

## Usage


## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
